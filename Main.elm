module Main exposing (..)

import Html exposing (..)
import Http
import Json.Decode exposing (Decoder, int, list, string)
import Json.Decode.Pipeline exposing (decode, hardcoded, optional, required)
import Markdown
import RemoteData exposing (RemoteData(..), WebData)
import RemoteData.Http


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


type alias Feed =
    { id : Int
    , title : String
    , url : String
    , image_url : String
    , last_checked : Int
    , added_at : Int
    , feed_entries : List FeedEntry
    }


decoderFeed : Decoder Feed
decoderFeed =
    decode Feed
        |> required "id" int
        |> required "title" string
        |> required "url" string
        |> required "image_url" string
        |> required "last_checked_at" int
        |> required "added_at" int
        |> required "feed_entries" (list decoderFeedEntry)


type alias FeedEntry =
    { id : Int
    , title : String
    , description : String
    , content : String
    , published_at : Int
    }


decoderFeedEntry : Decoder FeedEntry
decoderFeedEntry =
    decode FeedEntry
        |> required "id" int
        |> required "title" string
        |> required "description" string
        |> required "content" string
        |> required "published_at" int


type alias Model =
    { feeds : WebData (List Feed) }


type Msg
    = HandleFeedsResponse (WebData (List Feed))


loadFeeds =
    RemoteData.Http.get "http://localhost:8080/feeds/" HandleFeedsResponse (list decoderFeed)


init : ( Model, Cmd Msg )
init =
    ( { feeds = NotAsked }
    , loadFeeds
    )


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


view : Model -> Html Msg
view model =
    div [] [ viewFeeds model.feeds ]


viewFeeds feeds =
    case feeds of
        Success feeds_ ->
            div []
                [ h2 [] [ text ("Loaded " ++ (List.length feeds_ |> toString) ++ " feeds") ]
                , div [] (List.map viewFeed feeds_)
                ]

        Loading ->
            h2 [] [ text "Loading..." ]

        Failure _ ->
            h2 [] [ text "Error..." ]

        NotAsked ->
            h2 [] [ text "Not asked" ]


viewFeed feed =
    div [] <|
        List.map viewFeedEntry feed.feed_entries


viewFeedEntry feed_entry =
    div [] [ Markdown.toHtml [] feed_entry.description ]


update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    case action of
        HandleFeedsResponse result ->
            ( { model | feeds = result }, Cmd.none )
